# ss-rank

rank ss (shadowsocks) proxies by download speed 


At first, put some links (ss url) in sslinks.list, and then run ss-loader.py
`python3 ss-loader.py`

at the end, you can prefer the largest file (name of the file is equal to number of line in the _sslinks.list_)
or use `ls -lhS *.test_zip`

**Note** you can save the result of __ls__ in a file and delete all __*.test_zip__
