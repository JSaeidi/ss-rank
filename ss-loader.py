import os
from time import sleep
SLEEP_TIME = 15
DOWNLOAD_LINK = ""
urls = []

#creating config file:
def create_conf(server_address, server_port, local_port, password, method):
    config=[]
    config.append('{')
    config.append(f'''    "server":"{server_address}",''')
    config.append(f'''    "server_port":{server_port},''')
    config.append(f'''    "local_port":{local_port},''')
    config.append(f'''    "password":"{password}",''')
    config.append(f'''    "method":"{method}"''')
    config.append('}')

    config = '\n'.join(config)

    with open("ss_config.json", 'w') as config_file:
        config_file.write(config)


# loading ss urls
with open('sslinks.list' , 'r') as f:
    for this_line in f.readlines():
        if this_line.strip():
            urls.append(this_line.strip())
os.system("rm *.test_zip")
for counter,each_url in enumerate(urls):
    number = counter + 1
    print(f"----> {number} / {len(urls)}   |||   ", each_url)
    hash, server_info = (each_url.split("ss://")[1]).split("@")
    server_address, server_port = server_info.split("#")[0].split(":")
    tag = server_info.split("#")[1]
#    print(server_address, '\n', server_port, '\n', hash, '\n', tag)
    hash = os.popen(f'''echo "{hash.strip()}" |base64 -d''').read()
    method, password = hash.split(":")
#    print( server_address, '\n', server_port, '\n', hash, '\n', tag, '\n', method, '\n', password)
    create_conf(server_address, server_port, 1378, password, method)
    os.system(f"ss-local -c ./ss_config.json&sleep 5; curl --socks5-hostname 127.0.0.1:1378 http://speedtest.tele2.net/1GB.zip --output {number}.test_zip & sleep {SLEEP_TIME}; killall ss-local")
    #exit()
    sleep(1)


os.system("ls -Slh *.test_zip")
